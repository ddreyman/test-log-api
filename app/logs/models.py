from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Log(db.Model):
    __bind_key__ = 'logs'
    __tablename__ = "logs"

    id = db.Column(db.Integer, primary_key=True)
    dt = db.Column(db.Float)
    event = db.Column(db.Text)


class Person(db.Model):
    __bind_key__ = 'people'
    __tablename__ = "people"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.Text)
    last_name = db.Column(db.Text)
    gender = db.Column(db.Text)
